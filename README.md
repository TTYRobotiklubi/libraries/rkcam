# RkCam

## Requirements

* CMake >= 3.14
* OpenCV >= 4.0.0
* Boost - Used only when compiled with `WITH_SIGNALS`
* Catch2 - Used only for unit testing.

## Configuring

The library should be developed in a manner where specific hardware implementations can be turned off in the compilation.
Which means that the library should be exportable as an interface only deal. As such, options will exist to enable different
hardware implementations for various common camera modules.

These options include:
* `WITH_OPENCV_CAM` - Enable the compilation with an `ÌCamera` wrapper for OpenCV's `VideoCapture` source.
* `WITH_SIGNALS` - Enables `boost::signals2` support for the `CameraPoller` class. Requires Boost.

## Integration Into Another Project

There are two main ways to include this project in your own. The first is to use it as a **subdirectory**,
perhaps with git submodules; the other is to use the **find_package** module in CMake.

### As a Package

The, perhaps preferable way, is to use the `find_package` module of CMake.

First, download this project somewhere and install it. Note that, by default, it will attempt to install
into system folders. To change this, supply the initial `cmake` command with an argument of `-DCMAKE_INSTALL_PREFIX=some/path`,
where the path indicated is the path where all of your libraries will be installed. A good idea might be to keep a libraries
folder per final project. Then simply run `make install`.

After this, the files will be installed under the `some/path` directory. You can then provide the exact same argument to
your own project's CMake configuration and then use `find_package(RkCam REQUIRED)` to locate the library. An example of 
the installation process might look like this:

```
$ mkdir _libs // The folder where we install all of our compiled dependencies.
$ git clone https://gitlab.com/TTYRobotiklubi/libraries/rkcam.git ./rkcam_lib
$ cd rkcam_lib
$ mkdir build && cd build
$ cmake -DCMAKE_INSTALL_PREFIX=../../_libs ..
$ make install // This will now compile the library and install it into the _libs folder
$
$ cd ../../my_project
$ mkdir build && cd build
$ cmake -DCMAKE_PREFIX_PATH=../../_libs
$ make // This will build your application and link it with the library.
```

An accompanying CMake for the example project might look like this:
```cmake
cmake_minimum_required(VERSION 3.14)
project(my_project)

find_package(RkCam REQUIRED)

# your own project setup goes here...

target_link_libraries(my_target
    PUBLIC
        RkCam::RkCamLib
)
```

### As a Subdirectory

To include the project as a subdirectory, simply include the project in your own project's source directory
somehow (with git submodules, or by directly downloading and pasting), and then utilize the `add_subdirectory`
command to add the project to your own.

Doing this will import the project's targets into your own project, and you can link against the project
as you see fit.

Note that the order in which the subdirectories are added is important: the library's must be added before
the library itself is referenced.

An example using this system would go as follows:

```cmake
cmake_minimum_required(VERSION 3.14)
project(my_project)

add_subdirectory(path_to_rkcam)

# your own project setup goes here...

target_link_libraries(my_target
    PUBLIC
        RkCam::RkCamLib
)
``` 
