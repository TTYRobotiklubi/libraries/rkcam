#ifndef RKCAM_CAMERA_STUB_HPP_
#define RKCAM_CAMERA_STUB_HPP_

#include <rkcam/icamera.hpp>

namespace
{

struct CameraStub : public RkCam::ICamera
{
  cv::Size size;
  int type;

  std::function<outcome::result<void> (cv::Mat&)> capture_holder;
  std::function<outcome::result<void> ()> start_holder;
  std::function<void ()> stop_holder;
  std::function<void ()> destructor_holder;

  ~CameraStub()
  {
    if (destructor_holder)
      destructor_holder();
  }

  cv::Size FrameSize() const override
  {
    return size;
  }

  int FrameType() const override
  {
    return type;
  }

  outcome::result<void> StartCapture() override
  {
    if (start_holder)
      return start_holder();

    return outcome::success();
  }

  void StopCapture() override
  {
    if (stop_holder)
      stop_holder();
  }

  outcome::result<void> CaptureFrame(cv::Mat& mat) override
  {
    if (capture_holder)
      return capture_holder(mat);

    return outcome::success();
  }
};

}

#endif //RKCAM_CAMERA_STUB_HPP_
