#include <iostream>
#include <catch2/catch.hpp>

#include "stubs/camera_stub.hpp"

#include <rkcam/camera_poller.hpp>

SCENARIO("CameraPoller handles the camera as expected", "[camera_poller]")
{
  RkCam::CameraPoller poller(std::make_unique<CameraStub>());

  CameraStub& camera = dynamic_cast<CameraStub&>(poller.Camera().operator*());

  GIVEN("An initialized CameraPoller")
  {
    WHEN("CameraPoller is started and camera returns no error")
    {
      const outcome::result<void> res = poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT);

      THEN("Start returns no error")
      {
        REQUIRE(!res.has_error());
      }

      THEN("CameraPoller returns IsRunning as true")
      {
        REQUIRE(poller.IsRunning());
      }
    }

    WHEN("CameraPoller is started and camera returns an error")
    {
      const RkCam::CameraOperationError error = RkCam::CameraOperationError::Generic;

      camera.start_holder = [&error]
        {
          return error;
        };

      const outcome::result<void> result = poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT);

      THEN("CameraPoller returns the same error")
      {
        REQUIRE(result.has_error());
        REQUIRE(result.error() == error);
      }

      THEN("CameraPoller reports IsRunning as false")
      {
        REQUIRE(!poller.IsRunning());
      }
    }
  }

  GIVEN("A running CameraPoller")
  {
    REQUIRE(poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT));

    WHEN("CameraPoller is stopped")
    {
      std::atomic<bool> was_stopped = false;
      camera.stop_holder = [&was_stopped]
        {
          was_stopped = true;
        };

      poller.Stop();

      THEN("CameraPoller returns IsRunning as false")
      {
        REQUIRE(!poller.IsRunning());
      }

      THEN("Camera was stopped as well")
      {
        REQUIRE(was_stopped);
      }
    }

    WHEN("CameraPoller is attempted to start again")
    {
      std::atomic<bool> attempted_restart = false;
      camera.start_holder = [&attempted_restart]
        {
          attempted_restart = true;

          return outcome::success();
        };

      const outcome::result<void> res = poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT);

      THEN("No error is returned")
      {
        REQUIRE(!res.has_error());
      }

      THEN("Camera is not started for a second time")
      {
        REQUIRE(!attempted_restart);
      }
    }
  }
}

SCENARIO("CameraPoller is able to retrieve frames as expected", "[camera_poller]")
{
  RkCam::CameraPoller poller(std::make_unique<CameraStub>());

  CameraStub& camera = dynamic_cast<CameraStub&>(poller.Camera().operator*());

  camera.size = {10, 10};
  camera.type = CV_8U;

  GIVEN("a filled frame to serve")
  {
    cv::Mat mat_to_serve(camera.size, camera.type, cv::Scalar::all(2));
    std::atomic<bool> frame_copied = false;

    camera.capture_holder = [&mat_to_serve, &frame_copied](cv::Mat& mat)
      {
        mat_to_serve.copyTo(mat);

        frame_copied = true;

        return outcome::success();
      };

    REQUIRE(poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT));

    while (!frame_copied);  // Necessary to avoid race conditions!

    WHEN("a frame is requested")
    {
      outcome::result<cv::Mat> frame_retrieved = poller.GetFrame();

      THEN("no error is returned")
      {
        REQUIRE(frame_retrieved.has_value());
      }

      THEN("the frame has the proper format")
      {
        cv::Mat& mat_retrieved = frame_retrieved.value();

        REQUIRE(mat_retrieved.size() == mat_to_serve.size());
        REQUIRE(mat_retrieved.type() == mat_to_serve.type());
      }

      THEN("the returned frame contains proper data")
      {
        cv::Mat& mat_retrieved = frame_retrieved.value();

        REQUIRE(
            std::equal(
                mat_retrieved.begin<char>(),
                mat_retrieved.end<char>(),
                mat_to_serve.begin<char>()
                    )
                  );
      }
    }
  }

  GIVEN("a camera that serves an error")
  {
    const RkCam::CameraOperationError error = RkCam::CameraOperationError::CameraNotInitialized;
    std::atomic<bool> frame_copied = false;

    camera.capture_holder = [&frame_copied, &error](cv::Mat&)
      {
        frame_copied = true;

        return error;
      };

    REQUIRE(poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT));

    while (!frame_copied);

    WHEN("a frame is requested")
    {
      outcome::result<cv::Mat> result = poller.GetFrame();

      THEN("result contains the specified error")
      {
        REQUIRE(result.has_error());

        REQUIRE(result.error() == error);
      }
    }
  }
}


