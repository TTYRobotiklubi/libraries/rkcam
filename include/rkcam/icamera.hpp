#ifndef RKCAM_ICAMERA_HPP_
#define RKCAM_ICAMERA_HPP_

#include <memory>
#include <opencv2/opencv.hpp>

#include "camera_error_code.hpp"

namespace RkCam
{

class ICamera
{
public:
  using SPtr = std::shared_ptr<ICamera>;
  using UPtr = std::unique_ptr<ICamera>;

  using CSPtr = std::shared_ptr<const ICamera>;
  using CUPtr = std::unique_ptr<const ICamera>;

  virtual cv::Size FrameSize() const = 0;
  virtual int FrameType() const = 0;

  virtual outcome::result<void> StartCapture() = 0;
  virtual void StopCapture() = 0;

  virtual outcome::result<void> CaptureFrame(cv::Mat&) = 0;
};

}

#endif // RKCAM_ICAMERA_HPP_