#ifndef RKCAM_OPENCV_CAMERA_HPP_
#define RKCAM_OPENCV_CAMERA_HPP_

#ifdef WITH_OPENCV_CAM

#include "icamera.hpp"

namespace RkCam
{

class OpenCvCamera : public ICamera
{
public:
  static std::unique_ptr<OpenCvCamera> FromCamera(const int index = 0);
  static std::unique_ptr<OpenCvCamera> FromFile(const std::string& filename);

  explicit OpenCvCamera(const int index);
  explicit OpenCvCamera(const std::string& filename);
  ~OpenCvCamera();

  cv::Size FrameSize() const override;
  int FrameType() const override;

  outcome::result<void> StartCapture() override;
  void StopCapture() override;

  outcome::result<void> CaptureFrame(cv::Mat& mat) override;

private:
  enum CvInitializerType
  {
    FromString,
    FromInt
  };

  cv::VideoCapture _capture_device;
  cv::Size _frame_size;
  int _frame_type = 0;

  const CvInitializerType _initializer_type;
  const int _device_id = 0;
  const std::string _file_name = "";

  void _CacheParams();
};

}

#else

#warning Set the CMake option -DWITH_OPENCV_CAM to compile this header and its library.

#endif //WITH_OPENCV_CAM

#endif //RKCAM_OPENCV_CAMERA_HPP_
