#ifndef RKCAM_CAMERA_ERROR_CODE_HPP_
#define RKCAM_CAMERA_ERROR_CODE_HPP_

#include <iostream>
#include <string>
#include <system_error>

#include <boost/outcome.hpp>

namespace outcome = BOOST_OUTCOME_V2_NAMESPACE;

namespace RkCam
{

enum class CameraOperationError
{
  Success = 0,
  CameraNotInitialized = 1,
  CameraNotFound = 2,
  Generic = 3
};

}

namespace boost::system
{

template<>
struct is_error_code_enum<RkCam::CameraOperationError> : std::true_type
{};

}

namespace detail
{

class CameraOperationError_Category : public boost::system::error_category
{
public:
  virtual const char* name() const noexcept override final
  {
    return "CameraOperationError";
  }

  virtual std::string message(int code) const override final
  {
    switch (static_cast<RkCam::CameraOperationError>(code))
    {
    case RkCam::CameraOperationError::Success:
      return "capture successful";
    case RkCam::CameraOperationError::CameraNotInitialized:
      return "camera not initialized";
    case RkCam::CameraOperationError::CameraNotFound:
      return "camera device not found";
    case RkCam::CameraOperationError::Generic:
      return "generic camera error";
    default:
      return "unknown";
    }
  }

  const static CameraOperationError_Category& get()
  {
    const static CameraOperationError_Category cat;
    return cat;
  }
};

}
namespace RkCam
{

inline boost::system::error_code make_error_code(CameraOperationError e)
{
  return {static_cast<int>(e), detail::CameraOperationError_Category::get()};
}
}

#endif //RKCAM_CAMERA_ERROR_CODE_HPP_
