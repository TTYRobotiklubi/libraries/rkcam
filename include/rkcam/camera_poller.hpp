#ifndef RKCAM_CAMERA_POLLER_HPP_
#define RKCAM_CAMERA_POLLER_HPP_

#include <thread>
#include <mutex>
#include <atomic>
#include <opencv2/opencv.hpp>

#ifdef WITH_SIGNALS
# include <boost/signals2.hpp>
#endif // WITH_SIGNALS

#include "icamera.hpp"

namespace RkCam
{

class CameraPoller
{
public:
  const static int POLLING_INTERVAL_INSTANT = 0;

  CameraPoller(ICamera::UPtr&& camera);
  ~CameraPoller();

  const ICamera::UPtr& Camera();

  outcome::result<void> Start(const int interval);
  void Stop();
  bool IsRunning() const;

  outcome::result<cv::Mat> GetFrame();

#ifdef WITH_SIGNALS
  using Callback_OnNewFrame = std::function<void (outcome::result<cv::Mat> capture_status)>;

  boost::signals2::connection ConnectOnNewFrame(Callback_OnNewFrame cb);
#endif // WITH_SIGNALS

private:
  enum FrameIndicator
  {
    FrameA,
    FrameB
  };

  struct FrameHolder
  {
    outcome::result<void> capture_error = outcome::success();
    cv::Mat frame;
  };

  ICamera::UPtr _camera;

  std::thread _camera_worker;
  std::atomic<bool> _working;
  int _polling_interval;

  std::mutex _frame_lock;
  FrameIndicator _currently_serving;

  FrameHolder _frame_a;
  FrameHolder _frame_b;

#ifdef WITH_SIGNALS
  boost::signals2::signal<void (outcome::result<cv::Mat>)> _on_new_frame;
  void _EmitOnNewFrame(FrameHolder& frame);
#endif // WITH_SIGNALS

  cv::Mat _ConstructFrame();

  void _WorkerFunction();

  FrameHolder& _GetNextFrame();
  outcome::result<void> _RetreiveFrameFromCamera(FrameHolder& indicator_pair);
  void _UpdateServedFrame();
};

}

#endif // RKCAM_CAMERA_POLLER_HPP_