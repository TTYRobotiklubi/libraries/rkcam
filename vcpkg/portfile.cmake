include(vcpkg_common_functions)

vcpkg_from_git(
        URL https://gitlab.com/TTYRobotiklubi/libraries/rkcam.git
        OUT_SOURCE_PATH SOURCE_PATH
        REF 7bfd51505c6c3aa83b11efbaf3997e548fbcc13b
)

vcpkg_check_features(
        OUT_FEATURE_OPTIONS FEATURE_OPTIONS
        signals WITH_SIGNALS
        opencvcam WITH_OPENCV_CAM
)

vcpkg_configure_cmake(
        SOURCE_PATH ${SOURCE_PATH}
        PREFER_NINJA
        OPTIONS
        ${FEATURE_OPTIONS}
)

vcpkg_install_cmake()

vcpkg_fixup_cmake_targets(CONFIG_PATH "cmake")

file(INSTALL ${SOURCE_PATH}/LICENSE DESTINATION ${CURRENT_PACKAGES_DIR}/share/rkcam RENAME copyright)
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)
