cmake_minimum_required(VERSION 3.14)

add_executable(webcam_with_poller
        main.cpp)

set_target_properties(webcam_with_poller
        PROPERTIES
            CXX_EXTENSIONS NO
)

target_link_libraries(webcam_with_poller
        PUBLIC
            RkCam::RkCamLib
)
