#include <iostream>

#include <rkcam/opencv_camera.hpp>
#include <rkcam/camera_poller.hpp>

#include <opencv2/highgui.hpp>

int main()
{
  RkCam::ICamera::UPtr cam = RkCam::OpenCvCamera::FromCamera(0);
  RkCam::CameraPoller poller(std::move(cam));

  if (const auto res = poller.Start(RkCam::CameraPoller::POLLING_INTERVAL_INSTANT); res.has_error())
    return 0;

  while (true)
  {
    if (auto res = poller.GetFrame())
    {
      cv::Mat& frame = res.value();

      if (frame.empty())
      {
        std::cout << "Empty frame" << std::endl;
        continue;
      }

      cv::imshow("Frame", res.value());

      const char c = static_cast<char>(cv::waitKey(1));

      if (c == 27)
        break;
    }
    else
    {
      std::cout << "Error." << std::endl;
    }
  }

  poller.Stop();
}