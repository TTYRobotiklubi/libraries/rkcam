#include <rkcam/camera_poller.hpp>

namespace RkCam
{

CameraPoller::CameraPoller(ICamera::UPtr&& camera)
  : _camera(std::forward<ICamera::UPtr>(camera))
  , _working(false)
  , _currently_serving(FrameA)
  , _frame_a{}
  , _frame_b{}
{}

CameraPoller::~CameraPoller()
{
  if (IsRunning())
    Stop();
}

const ICamera::UPtr& CameraPoller::Camera()
{
  return _camera;
}

outcome::result<void> CameraPoller::Start(const int interval)
{
  if (IsRunning())
    return outcome::success();

  if (outcome::result<void> res = _camera->StartCapture(); !res)
  {
    return res;
  }

  _working = true;
  _polling_interval = interval;

  _frame_a.frame = _ConstructFrame();
  _frame_b.frame = _ConstructFrame();

  _camera_worker = std::thread([this] { _WorkerFunction(); });

  return outcome::success();
}

void CameraPoller::Stop()
{
  if (!IsRunning())
    return;

  _working = false;
  _camera_worker.join();
  _camera->StopCapture();
}

bool CameraPoller::IsRunning() const
{
  return _working;
}

outcome::result<cv::Mat> CameraPoller::GetFrame()
{
  auto get_served_frame = [this]() -> FrameHolder&
  {
    if (_currently_serving == FrameA)
      return _frame_a;
    else
      return _frame_b;
  };

  std::unique_lock<std::mutex> lock(_frame_lock);

  FrameHolder& served_frame = get_served_frame();

  if (served_frame.capture_error.has_error())
    return served_frame.capture_error.error();
  else
    return served_frame.frame.clone();
}

#ifdef WITH_SIGNALS
boost::signals2::connection CameraPoller::ConnectOnNewFrame(Callback_OnNewFrame cb)
{
  return _on_new_frame.connect(cb);
}

void CameraPoller::_EmitOnNewFrame(FrameHolder& frame)
{
  if (!frame.capture_error.has_error())
    _on_new_frame(frame.frame);
  else
    _on_new_frame(frame.capture_error.error());
}
#endif // WITH_SIGNALS

cv::Mat CameraPoller::_ConstructFrame()
{
  return cv::Mat(_camera->FrameSize(), _camera->FrameType(), cv::Scalar::all(0));
}

void CameraPoller::_WorkerFunction()
{
  while (IsRunning())
  {
    FrameHolder& to_fill = _GetNextFrame();

    to_fill.capture_error = _RetreiveFrameFromCamera(to_fill);

    _UpdateServedFrame();

#ifdef WITH_SIGNALS
    _EmitOnNewFrame(to_fill);
#endif // WITH_SIGNALS

    std::this_thread::sleep_for(std::chrono::milliseconds(_polling_interval));
  }
}

CameraPoller::FrameHolder& CameraPoller::_GetNextFrame()
{
  if (_currently_serving == FrameA)
    return _frame_b;
  else
    return _frame_a;
}

outcome::result<void> CameraPoller::_RetreiveFrameFromCamera(FrameHolder& frame_pair)
{
  return _camera->CaptureFrame(frame_pair.frame);
}

void CameraPoller::_UpdateServedFrame()
{
  std::unique_lock<std::mutex> lock(_frame_lock);

  if (_currently_serving == FrameA)
    _currently_serving = FrameB;
  else
    _currently_serving = FrameA;
}

}
