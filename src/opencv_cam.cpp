#include <rkcam/opencv_camera.hpp>

#include <iostream>

namespace RkCam
{

std::unique_ptr<OpenCvCamera> OpenCvCamera::FromCamera(const int index)
{
  return std::make_unique<OpenCvCamera>(index);
}

std::unique_ptr<OpenCvCamera> OpenCvCamera::FromFile(const std::string& filename)
{
  return std::make_unique<OpenCvCamera>(filename);
}

OpenCvCamera::OpenCvCamera(const int index)
  : _capture_device()
  , _initializer_type(FromInt)
  , _device_id(index)
{}

OpenCvCamera::OpenCvCamera(const std::string& filename)
  : _capture_device()
  , _initializer_type(FromString)
  , _file_name(filename)
{}

OpenCvCamera::~OpenCvCamera()
{
  if (_capture_device.isOpened())
    StopCapture();
}

cv::Size OpenCvCamera::FrameSize() const
{
  if (!_capture_device.isOpened())
    return {0, 0};

  return _frame_size;
}

int OpenCvCamera::FrameType() const
{
  if (!_capture_device.isOpened())
    return 0;

  return _frame_type;
}

outcome::result<void> OpenCvCamera::StartCapture()
{
  if (_capture_device.isOpened())
    return CameraOperationError::Generic;

  bool succeeded;

  if (_initializer_type == FromString)
    succeeded = _capture_device.open(_file_name);
  else
    succeeded = _capture_device.open(_device_id);

  if (!succeeded)
    return CameraOperationError::Generic;

  _CacheParams();

  return outcome::success();
}

void OpenCvCamera::StopCapture()
{
  if (!_capture_device.isOpened())
    return;

  _capture_device.release();
}

outcome::result<void> OpenCvCamera::CaptureFrame(cv::Mat& mat)
{
  if (!_capture_device.isOpened())
    return CameraOperationError::CameraNotInitialized;

  if (!_capture_device.read(mat))
    return CameraOperationError::Generic;

  return outcome::success();
}

void OpenCvCamera::_CacheParams()
{
  _frame_size.height = static_cast<int>(_capture_device.get(cv::CAP_PROP_FRAME_HEIGHT));
  _frame_size.width = static_cast<int>(_capture_device.get(cv::CAP_PROP_FRAME_WIDTH));

  _frame_type = static_cast<int>(_capture_device.get(cv::CAP_PROP_FORMAT));
}

}
